iptables -F &&
iptables -P INPUT DROP &&
iptables -P OUTPUT DROP &&
iptables -P FORWARD DROP &&
iptables -A INPUT -s 172.18.0.0/16 -j ACCEPT &&
iptables -A OUTPUT -s 172.18.0.0/16 -j ACCEPT &&
/etc/init.d/iptables save &&
/etc/init.d/iptables restart &&
iptables -L &&
chkconfig iptables on
